## CI Templates  

This project is intended to be referenced as a template for different pipeline types.  

## Tagging  
This project is tagged from a "tag" job in the pipeline. You will need to update the `version.json` file with the version for the git tag.  
